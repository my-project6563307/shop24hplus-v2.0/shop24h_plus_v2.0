$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo url
    var gUrlOffice = "http://localhost:8080/shop24h/offices";

    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";

    // Lấy token
    const vtoken = getCookie("token");

    // Cấu hình data table
    var vDataTable = $('#office-table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            columns: [
                {data: "id"},
                {data: "address_line"},
                {data: "city"},
                {data: "country"},
                {data: "phone"},
            ]
    });
    // Lưu id office dc chọn
    var gIdOffice = ""; 
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Tạo sự kiện logout
    $(document).on("click", "#btn-logout", function(){
        redirectToLogin();
    })
    
    // Tạo sự kiện nhấn dòng trên bảng
    $(document).on("click", "#office-table td", function(){
        onBtnRowClick(this);
        $("#myModal").modal("show");
    })

    // Tạo sự kiến nhấn nút thêm office
    $(document).on("click", "#btn-add", function(){
        onBtnAddClick();
        $("#myModal").modal("show");
    })

    // Tạo sự kiến nhấn nút thêm office
    $(document).on("click", "#btn-add-office", function(){
        onBtnAddOfficeClick();
    })
    // Tạo sự kiến nhấn nút cập nhật office
    $(document).on("click", "#btn-update", function(){
        onBtnUpdateOfficeClick();
    })
    // Tạo sự kiến nhấn xác nhận xoá office
    $(document).on("click", "#btn-delete", function(){
        $("#delete-modal").modal("show");
    })
    // Tạo sự kiến nhấn xác nhận xoá office
    $(document).on("click", "#btn-confirm-delete", function(){
        onBtnDeleteOfficeClick();
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();
        
    }
    // Hàm xử lí nhấn nút đổi hình ảnh
    function uploadImage() {
        var fileInput = document.getElementById("inp-photo-file");
        const previewImage = document.getElementById('product-img');

        const selectedFile = fileInput.files[0];
        const reader = new FileReader();

        if (selectedFile) {
            reader.readAsDataURL(selectedFile);
        }
        reader.addEventListener('load', () => {
            previewImage.src = reader.result;
        });
    }
    // Hàm gọi api lấy toàn bộ sản phẩm
    function callApiGetAllOffice(){
        "use strict"
        var gUrlAllOffice = "http://localhost:8080/shop24h/api/noauth/offices";
        $.ajax({
            url: gUrlAllOffice,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                loadProductToTable(responseObject);
            },
            error: function(er) {
                // errorHandler();
            }
        });
    }
    // Hàm tạo mới sản phẩm
    function callApiCreateOffice(paramObj){
        "use strict"
        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: gUrlOffice,
            type: "POST",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Create a new office successully');
                callApiGetAllOffice();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật sản phẩm
    function callApiUpdateOffice(paramObj){
        "use strict"

        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: gUrlOffice + "/" + gIdOffice,
            type: "PUT",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Update the office successully');
                callApiGetAllOffice();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm xoá sản phẩm
    function callApiDeleteOffice(){
        "use strict"
        $.ajax({
            url: gUrlOffice + "/" + gIdOffice,
            type: "DELETE",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Delete the office successully');
                callApiGetAllOffice();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật sản phẩm
    function onBtnUpdateOfficeClick(){
        "use strict"
        var vOfficeObj = {
            address_line: "",
            city: "",
            country: "",
            phone: ""
        }
        // Thu thập dữ liệu
        readFormData(vOfficeObj);
        // Xử lí dữ liệu
        var vValid = validateData(vOfficeObj);
        if(vValid){
            callApiUpdateOffice(vOfficeObj);
        }
    }
    // Hàm xoá sản phẩm
    function onBtnDeleteOfficeClick(){
        "use strict"
        $("#modal-delete").modal("hide");
        $("#myModal").modal("hide");
        callApiDeleteOffice();
    }
    // Hàm kiểm tra đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                if(responseObject.roles[0].name == "ROLE_MANAGER"){
                    callApiGetAllOffice();
                    responseHandler(responseObject);
                }
                else{
                    redirectToLogin();
                }
            },
            error: function(xhr) {
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }
    // Hàm xử lí khi nhấn row
    function onBtnRowClick(paramRow){
        "use strict"
        gIdOffice = "";
        var vDataRow = onRowClick(paramRow);
        if(vDataRow.length > 0){
            loadDataToForm(vDataRow);
        }
    }

    // Hàm xử lí nhấn xoá sản phẩm
    function onIconDeleteRowClick(paramIcon){
        gIdOffice = "";
        var vDataRow = onRowClick(paramIcon);
        gIdOffice = vDataRow[0].id;
        $("#modal-delete").modal("show");
    }
    // Hàm xử lí khi nhấn thêm product
    function onBtnAddClick(){
        "use strict"
        emptyForm();
    }
    // Hàm xử lí khi nhấn thêm sản phẩm
    function onBtnAddOfficeClick(){
        "use strict"
        var vOfficeObj = {
            address_line: "",
            city: "",
            country: "",
            phone: ""
        }
        // Thu thập dữ liệu
        readFormData(vOfficeObj);
        // Xử lí dữ liệu
        var vValid = validateData(vOfficeObj);
        if(vValid){
            callApiCreateOffice(vOfficeObj);
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm thu thập dữ liệu tạo mới product
    function readFormData(paramObj){
        "use strict"
        paramObj.address_line = $("#inp-address").val();
        paramObj.city = $("#inp-city").val();
        paramObj.country = $("#inp-country").val();
        paramObj.phone = $("#inp-phone").val();
    }
    // Hàm xử lí dữ liệu
    function validateData(paramObj){
        "use strict"
        if(paramObj.address_line == ""){
            toastr.error('Please fill out address');
            $("#inp-address").focus();
            return false;
        }
        if(paramObj.city == ""){
            toastr.error('Please fill out city');
            $("#inp-city").focus();
            return false;
        }
        if(paramObj.country == ""){
            toastr.error('Please fill out country');
            $("#inp-country").focus();
            return false;
        }
        if(paramObj.phone == ""){
            toastr.error('Please fill out phone');
            $("#inp-phone").focus();
            return false;
        }
    return true;
    }
    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau kiểm tra người dùng thành công
    function responseHandler(paramRes){
        "use strict"
        $("#username").html(paramRes.username);
        $("#user-img").attr("src", gUrlDownloadPhoto +"/"+ paramRes.photo);
    }
    // Hàm ghi dữ liệu vào bảng
    function loadProductToTable(paramRes){
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(paramRes);
        vDataTable.order( [ 0, 'desc' ] ).draw();
    }

    // Hàm lấy dữ liệu khi chọn row
    function onRowClick(paramRow){
        "use strict"
        var vRow = $(paramRow).closest("tr");
        $("#office-table tr").removeClass("active");
        vRow.addClass("active");
        var vDataOnRow = vDataTable.rows(vRow).data();
        return vDataOnRow;
    }
    // Hàm tải dữ liệu lên form
    function loadDataToForm(paramRowData){
        "use strict"
        $("#btn-action").html(`<button id="btn-update" class="btn btn-primary">Update</button>
        <button id="btn-delete" class="btn btn-danger">Delete</button>`);
        gIdOffice = paramRowData[0].id;
        $("#inp-id").removeClass("d-none");
        $("#label-id").removeClass("d-none");
        $("#inp-id").val(paramRowData[0].id);
        $("#inp-address").val(paramRowData[0].address_line);
        $("#inp-city").val(paramRowData[0].city);
        $("#inp-country").val(paramRowData[0].country);
        $("#inp-phone").val(paramRowData[0].phone);
    }
    // Hàm xoá dữ liệu trên form
    function emptyForm(){
        "use strict"
        $("#btn-action").html(`<button id="btn-add-office" class="btn btn-info">Add</button>`);
        gIdOffice = "";
        $("#inp-id").val("");
        $("#inp-id").addClass("d-none");
        $("#label-id").addClass("d-none");
        $("#inp-address").val("");
        $("#inp-city").val("");
        $("#inp-country").val("");
        $("#inp-phone").val("");

    }
})