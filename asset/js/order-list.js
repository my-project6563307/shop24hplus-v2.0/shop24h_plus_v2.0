$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo url
    var gUrlOrders = "http://localhost:8080/shop24h/orders";

    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gOrderDetailsUrl = "http://localhost:8080/shop24h/order-details/orders";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";

    var gUrlProductsDetails = "http://localhost:8080/shop24h/api/noauth/products/detail/" ;

    var gUrlProducts = "http://localhost:8080/shop24h/products";

    var gUrlCreateOrder = "http://localhost:8080/shop24h/api/noauth/orders";

    var gUrlOrderDetails = "http://localhost:8080/shop24h/api/noauth/order-details";

    var gUrlOrderDetailCreate = "http://localhost:8080/shop24h/api/noauth/order-details";

    var gUrlPayment = "http://localhost:8080/shop24h/payments";

    var gUrlOrderDetailsUpdate = "http://localhost:8080/shop24h/order-details/";

    var gUrlOrderUpdate = "http://localhost:8080/shop24h/orders/";

    var gUrlDateRangeFilter = "http://localhost:8080/shop24h/orders/filter";

    var gExcelExportUrl = "http://localhost:8080/shop24h/api/noauth/export/order/excel";
    // Lấy token
    const vtoken = getCookie("token");

    // Cấu hình data table
    var vDataTable = $('#order-table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            columns: [
                {data: "id"},
                {data: "status"},
                {data: "createPayment"},
                {data: "deliveryAddress"},
                {data: "comments"},
                {data: "orderDate"},
                {data: "requiredDate"},
                {data: "shippedDate"},
                {data: "usernameOfcustomer"}
            ],
            columnDefs: [
                {
                    targets: 1,
                    render: function(data){
                        if(data == "On hold"){
                            return `<p class="text-info">On hold</p>`
                        }
                        if(data == "Delivering"){
                            return `<p class="text-success">Delivering</p>`
                        }
                        if(data == "Shipped"){
                            return `<p class="text-warning">Shipped</p>`
                        }
                        if(data == "Canceled"){
                            return `<p class="text-danger">Canceled</p>`
                        }
                        else{
                            return "";
                        }
                    }
                },
                {
                    targets: 2,
                    render: function(data){
                        if(data == "no"){
                            return `<p class="text-danger">No</p>`
                        }
                        if(data == "yes"){
                            return `<p class="text-success">Yes</p>`
                        }
                        else{
                            return "";
                        }
                    }
                }
            ]
    });
    // Lưu id dòng sản phẩm dc chọn
    var gIdOrder = ""; 

    // Lưu trạng thái order đang được lọc

    var gStatusFilter = "all";

    //  Lưu ngày tháng lọc

    var gDate1 = "1970-01-01";
    
    var gCurrentDate = new Date();

    var gDate2 = new Date(gCurrentDate.getTime() + (1 * 24 * 60 * 60 * 1000)).toISOString().slice(0, 10);

    // Lưu sản phẩm được chọn
    var gProductSelected = [];

    // Lưu thông tin product dc chọn

    var gProductList = [];
    
    // Lưu thông tin tổng tiền cho đơn hàng được ship
    var gAmount = 0;

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Tạo sự kiện logout
    $(document).on("click", "#btn-logout", function(){
        redirectToLogin();
      })
      
    // Tạo sự kiện nhấn dòng trên bảng
    $(document).on("click", "#order-table td", function(){
        onBtnRowClick(this);
        $("#myModal").modal("show");
    })

    // Tạo sự kiến nhấn nút thêm order
    $(document).on("click", "#btn-add", function(){
        onBtnAddClick();
        $("#myModal").modal("show");
    })

    // Tạo sự kiến nhấn nút thêm order trên form
    $(document).on("click", "#btn-add-order", function(){
        onBtnAddOrderClick();
    })

    // Tạo sự kiến nhấn nút cập nhật order
    $(document).on("click", "#btn-update", function(){
        onBtnUpdateOrderClick("no");
    })

    // Tại sự kiện nhấn nút tạo payment
    $(document).on("click", "#btn-create-payment", function(){
        onBtnCreatePaymentClick();
    })
    // Tạo sự kiện chọn range ngày
    $(document).on("change", "#inp-date-range", function(){
        onChangeDateRange();
    })

    // Tạo sự kiện chọn range ngày
    $(document).on("change", "#select-customer", function(){
        var vCustomerId = $("#select-customer").val();
        callApiGetCustomerById(vCustomerId);
    })
    // Tạo sự kiện chọn range ngày
    $(document).on("change", "#select-product", function(){
        gProductSelected =  $("#select-product").val();
        onSelectChangeProduct();
    })
    // Tạo sự kiện lọc trạng thái
    $(document).on("click", "#btn-onhold", function(){
        $(".btn-filter").removeClass("actives");
        $("#btn-onhold").addClass("actives");
        gStatusFilter = "On hold";
        callApiGetOrderByDateRange();
    })
    $(document).on("click", "#btn-delivery", function(){
        $(".btn-filter").removeClass("actives");
        $("#btn-delivery").addClass("actives");
        gStatusFilter = "Delivering";
        callApiGetOrderByDateRange();
    })
    $(document).on("click", "#btn-shipped", function(){
        $(".btn-filter").removeClass("actives");
        $("#btn-shipped").addClass("actives");
        gStatusFilter = "Shipped";
        callApiGetOrderByDateRange();
    })
    $(document).on("click", "#btn-canceled", function(){
        $(".btn-filter").removeClass("actives");
        $("#btn-canceled").addClass("actives");
        gStatusFilter = "Canceled";
        callApiGetOrderByDateRange();
    })
    $(document).on("click", "#btn-all", function(){
        $(".btn-filter").removeClass("actives");
        $("#btn-all").addClass("actives");
        gStatusFilter = "all";
        callApiGetOrderByDateRange();
    })
    // Tạo sự kiện lọc theo ngày
    $(document).on("click", "#btn-date-range", function(){
        callApiGetOrderByDateRange();
    })
    // Tạo sự kiện xuất file excel
    $(document).on("click", "#btn-export", function(){
        callApigetexcel();
    })
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();
        //Date range picker
        $('#inp-date-range').daterangepicker();
        // Call api get customer
        callApiGetAllCustomers();

        //Khởi tạo select2 
        $('.select2bs4').select2({
        theme: 'bootstrap4'
        });
        // call APi get all product
        callApiGetAllProduct();
        
    }
    // Hàm thay đổi product
    function onSelectChangeProduct(){
        "use strict"
        if(gProductSelected.length > 0){
            $("#product-list").empty();
            for(let bIndex = 0; bIndex < gProductSelected.length; bIndex ++){
                callApiGetAllProductById(gProductSelected[bIndex]);
            }
        }
        else {
            $("#product-list").empty();
        }
    }
    // Hàm gọi api xuất excel
    function callApigetexcel(){
        "use strict"
        $.ajax({
        url: gExcelExportUrl + "?status=" + gStatusFilter
        + "&Date1=" + gDate1 + "&Date2=" + gDate2,
        type: "GET",
        "contentType": "application/octet-stream",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
            },
            success: function() {
            window.location.href = gExcelExportUrl + "?status=" + gStatusFilter
            + "&Date1=" + gDate1 + "&Date2=" + gDate2;
            },
            error: function(er) {
            console.log(er);
            }
        });
    }
    // Hàm gọi Api lấy danh sách sản phẩm
    function callApiGetAllProductById(paramId){
        "use strict"
        $.ajax({
            url: gUrlProductsDetails + paramId,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                loadProductToFormDisplay(responseObject);
            },
            error: function() {
                toastr.error('Failed to get products!');
            }
        });
    }
    // Hàm gọi api lấy toàn bộ dòng sản phẩm
    function callApiGetAllOrder(){
        $.ajax({
            url: gUrlOrders,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                loadProductToTable(responseObject);
            },
            error: function() {
                toastr.error('Failed to get orders!');
            }
        });
    }
    // Hàm tạo mới order
    function callApiCreateOrder(paramObj){
        "use strict"
        
        var vJsonObj = JSON.stringify(paramObj);

        $.ajax({
            url: gUrlCreateOrder + "?customerId=" + paramObj.customerId,
            type: "POST",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(res) {
                toastr.success('Create a new order for customer successully');
                callApiCreateOrderDetailsByOrderId(res.id);
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm tạo mới chi tiết đơn hàng với order id và danh sách product
    function callApiCreateOrderDetailsByOrderId(paramOrderId){
        "use strict"

        for(let bIndex = 0; bIndex < gProductSelected.length; bIndex++){
            let vOrderDetailsObj = {
                quantity_order: ""
            };
            vOrderDetailsObj.quantity_order = $("#inp-quantity-" + gProductSelected[bIndex] +"").val();
            let vJsonObj = JSON.stringify(vOrderDetailsObj)

            $.ajax({
                url: gUrlOrderDetails + "?orderId=" + paramOrderId + "&productId=" + gProductSelected[bIndex],
                type: "POST",
                contentType: "application/json",
                data: vJsonObj,
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function(res) {
                    toastr.success('Create a new order details for customer successully');
                    callApiGetAllOrder();
                },
                error: function(er) {
                    toastr.error(er.responseText);
                }
            });
        }
        
        
    }
    // Hàm tạo mới chi tiết đơn hàng với order id và product id
    function callApiCreateOrderDetails(paramOrderId, paramProductId){
        "use strict"

        // thu thập dữ liệu
        let vOrderDetailsObj = {
            quantity_order: ""
            }

        vOrderDetailsObj.quantity_order = $("#inp-quantity-" + paramProductId +"").val();
        
        // Chuyển đổi json data
        let vJsonObj = JSON.stringify(vOrderDetailsObj)

        // call ajax api
        $.ajax({
            url: gUrlOrderDetailCreate + "?orderId=" + paramOrderId + "&productId=" + paramProductId,
            type: "POST",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function() {
                toastr.success('Create a new order details for customer successully');
            },
            error: function(er) {
                toastr.error(er.responseText);
            }
        });   
    }
    // Hàm tạo mới payment
    function callApiCreatePayment(paramPaymentObj, paramCustomerId){
        "use strict"

        // Chuyển đổi json data
        let vJsonObj = JSON.stringify(paramPaymentObj)

        // call ajax api
        $.ajax({
            url: gUrlPayment + "?customerId=" + paramCustomerId,
            type: "POST",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function(res) {
                toastr.success('Create a payment for customer successfully');
                onBtnUpdateOrderClick("yes");
                if(gStatusFilter != "all"){
                    callApiGetOrderByDateRange();
                }
                else {
                    callApiGetAllOrder();
                }
            },
            error: function(er) {
                toastr.error(er.responseText);
            }
        });   
    }
    // Hàm cập nhật đơn hàng
    function callApiUpdateOrder(paramObj){
        "use strict"
        
        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: gUrlOrderUpdate + gIdOrder + "/customer/" + paramObj.customerId,
            type: "PUT",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(res) {
                orderDetailsHandler(res);
                toastr.success("Update order successfully!")
                if(gStatusFilter != "all"){
                    callApiGetOrderByDateRange();
                }
                else {
                    callApiGetAllOrder();
                }
            },
            error: function() {
                toastr.error('Error while updating order! Please try again.');
            }
        });
    }
    // Hàm thay đổi input date range
    function onChangeDateRange(){
        "use strict"
        var vDateRange = $('#inp-date-range').val();
        var dateValue1 = vDateRange.slice(0, 10)
        var dateValue2 = vDateRange.slice(13, 23)

        const year1 = dateValue1.substr(6,4);
        const month1 = dateValue1.substr(0, 2);
        const day1 = dateValue1.substr(3, 2);
        const year2 = dateValue2.substr(6,4);
        const month2 = dateValue2.substr(0, 2);
        const day2 = dateValue2.substr(3, 2);

        gDate1 = `${year1}-${month1}-${day1}`;
        gDate2 = `${year2}-${month2}-${day2 + " 23:59:59"}`;
    }

    // Hàm lọc order theo trạng thái và date range
    function callApiGetOrderByDateRange(){
        "use strict"
        $.ajax({
            url: gUrlDateRangeFilter + "?status=" + gStatusFilter
            + "&Date1=" + gDate1 + "&Date2=" + gDate2,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                toastr.success('Get orders successully');
                loadProductToTable(responseObject);
            },
            error: function(er) {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm cập nhật chi tiết đơn hàng qua id
    function callApiUpdateOrderDetailsById(paramId, paramProductId){
        "use strict"
        // Lấy số lượng cập nhật
        var vOrderDetailObj = {
            quantity_order : ""
        }
        vOrderDetailObj.quantity_order = $("#inp-quantity-"+ paramProductId +"").val();

        // Tạo data json
        var vJsonObj = JSON.stringify(vOrderDetailObj);
  
        // Ajax api cập nhật
        $.ajax({
            url: gUrlOrderDetailsUpdate + paramId +"/product/" +paramProductId,
            type: "PUT",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(res) {
                // success
            },
            error: function(er) {
                toastr.error(er.responseText);
            }
        });
    }
    // Hàm xoá chi tiết đơn hàng qua id
    function callApiDeleteOrderDetailsById(paramId, paramProductId){
        "use strict"

        // Url cập nhật cho chi tiết đơn hàng
        var vUrl = "http://localhost:8080/shop24h/order-details/" + paramId +"/product/" +paramProductId;
        
        // Ajax api cập nhật
        $.ajax({
            url: vUrl,
            type: "DELETE",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(res) {
                toastr.success("Delete order details successfully!");
            },
            error: function(er) {
                toastr.error(er.responseText);
            }
        });
    }
    
    // Hàm cập nhật order details / xoá orderdetail
    function orderDetailsHandler(paramRes){
        "use strict"
        // Ghi dữ liệu ra mảng mới
        let vCreateNewOrderList = [];
        gProductSelected.forEach(function(product){
            vCreateNewOrderList.push(product);
        })

        // Xử lí dữ liệu trong mảng
        for(let bIndex = 0; bIndex < gProductList. length; bIndex ++){
            if(vCreateNewOrderList.includes(gProductList[bIndex].product.id.toString())){
                callApiUpdateOrderDetailsById(gProductList[bIndex].id, gProductList[bIndex].product.id);
                vCreateNewOrderList.splice(vCreateNewOrderList.indexOf(gProductList[bIndex].product.id.toString()),1)
            }
            else {
                callApiDeleteOrderDetailsById(gProductList[bIndex].id, gProductList[bIndex].product.id);
            }
        }
        if(vCreateNewOrderList.length > 0){
            for(let bI = 0; bI < vCreateNewOrderList.length; bI ++){
                callApiCreateOrderDetails(paramRes.id, vCreateNewOrderList[bI]);
            }
        }
    }
    // Hàm cập nhật order
    function onBtnUpdateOrderClick(paramPayment){
        "use strict"
        // Update thông tin order
        var vUpdateObj = {
            status: "",
            comments: "",
            orderDate: "",
            requiredDate: "",
            shippedDate: "",
            deliveryAddress: "",
            createPayment: paramPayment
        }
        // Thu thập dữ liệu
        readFormData(vUpdateObj);
        // Xử lí dữ liệu
        var vValid = validateData(vUpdateObj);
        if(vValid){
            callApiUpdateOrder(vUpdateObj);
        }
    }
    // Hàm tạo hoá đơn từ đơn hàng
    function onBtnCreatePaymentClick(){
        "use strict"
        let vCustomerId = $("#select-customer").val();
        
        let vPaymentObj = {
            amount: "",
            paymentDate :""
        }
        vPaymentObj.amount = gAmount;
        vPaymentObj.paymentDate = new Date().toISOString().slice(0, 10);
        
        callApiCreatePayment(vPaymentObj, vCustomerId);

    }
    // Hàm xử lí khi nhấn thêm đơn hàng
    function onBtnAddOrderClick(){
        "use strict"
        var vCreateObj = {
            status: "",
            comments: "",
            orderDate: "",
            requiredDate: "",
            shippedDate: "",
            deliveryAddress: "",
            customerId: ""
        }
        // Thu thập dữ liệu
        readFormData(vCreateObj);
        // Xử lí dữ liệu
        var vValid = validateData(vCreateObj);

        if(gProductSelected.length == 0){
            toastr.error("Choose at least one product");
        }
        else if(vValid){
            callApiCreateOrder(vCreateObj);
        }
    }
    // Hàm kiểm tra đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                if(responseObject.roles[0].name == "ROLE_MANAGER"){
                    callApiGetAllOrder();
                    responseHandler(responseObject);
                }
                else{
                    redirectToLogin();
                }
            },
            error: function(xhr) {
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }
    // Hàm lấy all customer
    function callApiGetAllCustomers(){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/customers";

        $.ajax({
            url: vUrl,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(response) {
                handleResponseToSelect(response);
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm lấy customer by Id
    function callApiGetCustomerById(paramId){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/customers/" + paramId;

        $.ajax({
            url: vUrl,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(res) {
                handleResponseToInput(res);
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm lấy all product
    function callApiGetAllProduct(){
        "use strict"
        

        $.ajax({
            url: gUrlProducts + "?page=0&size=1000",
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(response) {
                handleResponseToSelectProduct(response);
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Get order details by order id
    function callApiGetOrderDetailsByOrderId(){
        $.ajax({
            url: gOrderDetailsUrl + "/admin/" + gIdOrder,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function(responseObject) {
                loadOrderDetailsToSelectProduct(responseObject);
            },
            error: function() {
            }
        });
    }
    // Hàm xử lí khi nhấn thêm product line
    function onBtnAddClick(){
        "use strict"
        // Reset form
        emptyForm();
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm xử lí load thông tin người dùng vào ô input địa chỉ
    function handleResponseToInput(paramRes){
        "use strict"
        $("#inp-address").val(paramRes.firstName +" " + paramRes.lastName 
        + ", " + paramRes.phoneNumber + ", " + paramRes.address + ", " + paramRes.city);
    }
    // Hàm xử lí load dữ liệu product vào form hiển thị
    function loadProductToFormDisplay(paramRes){
        "use strict"
        var vProductList = $("#product-list");
        let vProductItem = `
        <div class="product-item">
            <img src="`+ gUrlDownloadPhoto + "/" + paramRes.photo1 + `" alt="Product-photo">
            <p class="product-name">` + paramRes.productName + `</p>
            <input min = "1" max = "` + paramRes.quantityInStock + `" type="number" class="form-control" id = "inp-quantity-`+ paramRes.id +`">
        </div>`

        vProductList.append(vProductItem);

        $("#inp-quantity-"+ paramRes.id +"").val(1);

        for(let bIndex = 0; bIndex < gProductList.length; bIndex++){
            if(gProductList[bIndex].product.id == paramRes.id){
                $("#inp-quantity-"+ paramRes.id +"").val(parseInt(gProductList[bIndex].quantity_order));
                return;
            }
        }
    }
    // Hàm xử lí load dữ liệu product vào select
    function loadOrderDetailsToSelectProduct(paramRes){
        "use strict"
        var vSelectProduct = $("#select-product");
        $("#product-list").empty();

        // Xoá dữ liệu trước đó
        gProductSelected = [];

        // Khai báo biến lưu thông tin chi tiết đơn hàng
        gProductList = paramRes;

        // Xoá dữ liệu tổng tiền đơn hàng
        gAmount = 0;
        for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
            gProductSelected.push(paramRes[bIndex].product.id);
            gAmount += paramRes[bIndex].quantity_order * paramRes[bIndex].priceEach;
        }
        // load dữ liệu nhận được vào ô multiselect sản phẩm
        vSelectProduct.val(gProductSelected);
        vSelectProduct.change();
    }
    // Hàm xử lí load dữ liệu khách hàng vào ô select
    function handleResponseToSelect(paramRes){
        "use strict"
        var bSelectCustomer = $("#select-customer");
        for(let bIndex = 0; bIndex < paramRes.length; bIndex ++){
            let bOption = $("<option>");
            bOption.prop("value", (paramRes[bIndex].id))
            bOption.prop("text", (paramRes[bIndex].username) + " - " + paramRes[bIndex].phoneNumber)

            bSelectCustomer.append(bOption);
        }
    }
    // Hàm xử lí load dữ liệu sản phẩm vào ô select
    function handleResponseToSelectProduct(paramRes){
        "use strict"
        var bSelectProduct = $("#select-product");
        for(let bIndex = 0; bIndex < paramRes.length; bIndex ++){
            let bOption = $("<option>");
            bOption.prop("value", (paramRes[bIndex].id))
            bOption.prop("text", (paramRes[bIndex].productName))

            bSelectProduct.append(bOption);
        }
    }
    // Hàm xử lí khi nhấn row
    function onBtnRowClick(paramRow){
        "use strict"
        gIdOrder = "";
        //  lấy data và id lại cho row mới
        var vDataRow = onRowClick(paramRow);
        if(vDataRow.length > 0){
            loadDataToForm(vDataRow);
            callApiGetOrderDetailsByOrderId();
        }
    }
    // Hàm thu thập dữ liệu tạo mới 
    function readFormData(paramObj){
        "use strict"
        paramObj.status = $("#select-status").val();
        paramObj.comments = $("#inp-comment").val();
        paramObj.deliveryAddress = $("#inp-address").val();
        paramObj.orderDate = $("#inp-order-date").val();
        paramObj.requiredDate = $("#inp-required-date").val();
        paramObj.shippedDate = $("#inp-shipped-date").val();
        paramObj.customerId = $("#select-customer").val();
    }
    // Hàm xử lí dữ liệu
    function validateData(paramObj){
        "use strict"
        if(paramObj.status == ""){
            toastr.error('Please choose status');
            $("#select-status").focus();
            return false;
        }
        if(paramObj.status == "Delivering" && paramObj.requiredDate == ""){
            toastr.error('Please choose delivery date');
            $("#inp-required-date").focus();
            return false;
        }
        if(paramObj.status == "Shipped" && paramObj.shippedDate == ""){
            toastr.error('Please choose shipped date');
            $("#inp-shipped-date").focus();
            return false;
        }
        if(paramObj.status == "On hold" && paramObj.orderDate == ""){
            toastr.error('Please choose order date');
            $("#inp-order-date").focus();
            return false;
        }
        else if(paramObj.status == "On hold" && paramObj.orderDate != "") {
            paramObj.shippedDate = "";
            paramObj.requiredDate ="";
        }
        if(paramObj.deliveryAddress == ""){
            toastr.error('Please fill out delivery address');
            $("#inp-address").focus();
            return false;
        }
        if(paramObj.deliveryAddress == ""){
            toastr.error('Please fill out delivery address');
            $("#inp-address").focus();
            return false;
        }
    return true;
    }
    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau kiểm tra người dùng thành công
    function responseHandler(paramRes){
        "use strict"
        $("#username").html(paramRes.username);
        $("#user-img").attr("src", gUrlDownloadPhoto +"/"+ paramRes.photo);
    }
    // Hàm ghi dữ liệu vào bảng
    function loadProductToTable(paramRes){
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(paramRes);
        vDataTable.order( [ 0, 'desc' ] ).draw();
    }

    // Hàm lấy dữ liệu khi chọn row
    function onRowClick(paramRow){
        "use strict"
        var vRow = $(paramRow).closest("tr");
        $("#order-table tr").removeClass("active");
        vRow.addClass("active");
        var vDataOnRow = vDataTable.rows(vRow).data();
        return vDataOnRow;
    }
    // Hàm tải dữ liệu lên form
    function loadDataToForm(paramRowData){
        "use strict"
        if(paramRowData[0].createPayment == "yes"){
            $("#btn-action").html(`<button id="btn-update" class="btn btn-primary">Update</button>`);
        }
        else if (paramRowData[0].createPayment == "no" && paramRowData[0].status == "Shipped"){
            $("#btn-action").html(`<button id="btn-update" class="btn btn-primary mr-2">Update</button>`
            + `<button id="btn-create-payment" class="btn btn-success">Confirm Payment</button>`
            );
        }
        else {
            $("#btn-action").html(`<button id="btn-update" class="btn btn-primary">Update</button>`);
        }
        gIdOrder = paramRowData[0].id;
        $("#inp-id").removeClass("d-none");
        $("#label-id").removeClass("d-none");
        $("#inp-id").val(paramRowData[0].id);
        $("#select-status").val(paramRowData[0].status);
        $("#inp-comment").val(paramRowData[0].comments);
        $("#inp-address").val(paramRowData[0].deliveryAddress);
        $("#inp-order-date").val(paramRowData[0].orderDate);
        $("#inp-required-date").val(paramRowData[0].requiredDate);
        $("#inp-shipped-date").val(paramRowData[0].shippedDate);
        $("#select-customer").val(paramRowData[0].idofCustomer);
    }
    // Hàm xoá dữ liệu trên form
    function emptyForm(){
        "use strict"
        $("#btn-action").html(`<button id="btn-add-order" class="btn btn-info">Add</button>`);
        gIdOrder = "";

        $("#inp-id").val("");
        $("#inp-id").addClass("d-none");
        $("#label-id").addClass("d-none");
        $("#select-status").val("");
        $("#inp-comment").val("");
        $("#inp-address").val("");
        $("#inp-order-date").val("");
        $("#inp-required-date").val("");
        $("#inp-shipped-date").val("");
        $("#select-customer").val("");

        $("#select-product").val([]);
        $("#select-product").change();
        $("#product-list").empty();
    }
})