$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Lấy token
    const vtoken = getCookie("token");

    // Url login checks
    const urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    const gUrlCustomer = "http://localhost:8080/shop24h/customers";

    const gProductNUmberUrl = "http://localhost:8080/shop24h/products/number";

    const gPaymentAmountUrl = "http://localhost:8080/shop24h/payments/amount";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";
    // tạo dữ liệu cho biểu đồ ngày
    var areaChartData = {
        labels: [],
        datasets: [
            {
                label: 'Profit ($)',
                backgroundColor: '#28a745',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: [ 0,0,0,0,0,0,0]
            }
        ]
    }
    // tạo dữ liệu cho biểu đồ tháng
    var areaChartDataMonth = {
        labels: ["", "", ""],
        datasets: [
            {
                label: 'Profit ($)',
                backgroundColor: '#007bff',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: [0, 0, 0]
            }
        ]
    }
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // Tạo sự kiện logout
    $(document).on("click", "#btn-logout", function(){
      redirectToLogin();
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
      "use strict"
      callApiCheckUser();
    }
    // Hàm kiểm tra đăng nhập
    function callApiCheckUser(){
      $.ajax({
          url: urlInfo,
          type: "GET",
          dataType: "json",
          headers: {
              "Authorization": "Bearer " + vtoken + "" 
            },
          success: function(responseObject) {
              if(responseObject.roles[0].name == "ROLE_MANAGER"){
                  responseHandler(responseObject);
                  callApiGetOrderByDateRange();
                  callApiGetAllCustomer();
                  callApiGetNumberProduct();
                  callApiGetAnnualProfit();
                  callApiGetDailyProfit();
                  callApiGetMonthlyProfit();
                  callApiGetCustomerNumberByProfit();
                  callApiGetNumberOrderByCutomer();

              }
              else{
                  redirectToLogin();
              }
          },
          error: function(xhr) {
              // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
              redirectToLogin();
          }
      });
  }
  // Hàm lọc order theo trạng thái và date range
  function callApiGetOrderByDateRange(){
    "use strict"
    var vUrl = "http://localhost:8080/shop24h/orders/filter";

    var currentDate = new Date();
    // Get the date of 7 days before the current date
    var sevenDaysAgo = new Date(currentDate.getTime() - (7 * 24 * 60 * 60 * 1000));

    // Format the date as a string
    var gDate1 = sevenDaysAgo.toISOString().slice(0, 10);

    var gDate2 = new Date(currentDate.getTime() + (1 * 24 * 60 * 60 * 1000)).toISOString().slice(0, 10);

    $.ajax({
        url: vUrl + "?status=all"
        + "&Date1=" + gDate1 + "&Date2=" + gDate2,
        type: "GET",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + vtoken + "" 
          },
        success: function(responseObject) {
          $("#order-number").html(responseObject.length);
        },
        error: function(er) {
        }
    });
  }
  // Hàm gọi api lấy toàn bộ customer
  function callApiGetAllCustomer(){
    "use strict"
    $.ajax({
        url: gUrlCustomer + "?page=0&size=20",
        type: "GET",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + vtoken + "" 
          },
        success: function(responseObject) {
            $("#customer-number").html(responseObject.length);
        },
        error: function(er) {
        }
    });
  }
  // Hàm gọi api lấy toàn bộ số lượng sản phẩm
  function callApiGetNumberProduct(){
    "use strict"
    $.ajax({
        url: gProductNUmberUrl,
        type: "GET",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + vtoken + "" 
          },
        success: function(responseObject) {
            $("#product-number").html(responseObject);
        },
        error: function(er) {
            // toastr.error("Error! fail to get data");
        }
    });
  }
  // Hàm gọi api lấy tổng tiền theo năm
  function callApiGetAnnualProfit(){
    "use strict"

    var currentDate = new Date();
    // Get first day of this year
    var firsDayOfYear = new Date(currentDate.getFullYear(),0,1);

    // Format the date as a string
    var gDate1 = firsDayOfYear.toISOString().slice(0, 10);

    var gDate2 = new Date(currentDate.getTime() + (1 * 24 * 60 * 60 * 1000)).toISOString().slice(0, 10);

    $.ajax({
        url: gPaymentAmountUrl + "?Date1=" + gDate1 + "&Date2=" + gDate2,
        type: "GET",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + vtoken + "" 
          },
        success: function(responseObject) {
            $("#payment-profit").html("$ " + responseObject);
        },
        error: function(er) {
            // toastr.error("Error! fail to get data");
        }
    });
  }
  // Hàm gọi api lấy tổng tiền theo ngày
  function callApiGetDailyProfit(){
    "use strict"
    var gPayment7daysUrl = "http://localhost:8080/shop24h/payments/week";
      $.ajax({
          url: gPayment7daysUrl,
          type: "GET",
          dataType: "json",
          headers: {
              "Authorization": "Bearer " + vtoken + "" 
            },
          success: function(responseObject) {
            loadResToBarChart(responseObject);
          },
          error: function(er) {
          }
      });
    }
  // Hàm gọi api lấy tổng tiền theo ngày
  function callApiGetMonthlyProfit(){
    "use strict"
    var gPaymentMonthUrl = "http://localhost:8080/shop24h/payments/month";
      $.ajax({
          url: gPaymentMonthUrl,
          type: "GET",
          dataType: "json",
          headers: {
              "Authorization": "Bearer " + vtoken + "" 
            },
          success: function(responseObject) {
            loadResToBarChartMonth(responseObject);
          },
          error: function(er) {
          }
      });
    }
  // Hàm gọi api lấy tổng tiền theo số lượng khách hàng
  function callApiGetCustomerNumberByProfit(){
    "use strict"
    var gPaymentByCustomerUrl = "http://localhost:8080/shop24h/payments/level";
      $.ajax({
          url: gPaymentByCustomerUrl,
          type: "GET",
          dataType: "json",
          headers: {
              "Authorization": "Bearer " + vtoken + "" 
            },
          success: function(responseObject) {
            loadResToBarChartLevelCustomer(responseObject);
          },
          error: function(er) {
          }
      });
    }
  // Hàm gọi api lấy tổng đơn hàng theo khách hàng
  function callApiGetNumberOrderByCutomer(){
    "use strict"
    var gOrderQuantityByCustomerUrl = "http://localhost:8080/shop24h/orders/quantity";
      $.ajax({
          url: gOrderQuantityByCustomerUrl,
          type: "GET",
          dataType: "json",
          headers: {
              "Authorization": "Bearer " + vtoken + "" 
            },
          success: function(responseObject) {
            loadResToBarChartOrderQuantity(responseObject);
          },
          error: function(er) {
          }
      });
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm logout
    function redirectToLogin() {
      // Trước khi logout cần xóa token đã lưu trong cookie
      setCookie("token", "", 1);
      window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau kiểm tra người dùng thành công
    function responseHandler(paramRes){
      "use strict"
      $("#username").html(paramRes.username);
      $("#user-img").attr("src",gUrlDownloadPhoto +"/"+ paramRes.photo);
  }
  // Hàm load dữ liệu tổng tiền đơn hàng theo tuần vào bar chart
  function loadResToBarChart(paramRes){
    "use strict"

    for(let bIndex = 0; bIndex < 7; bIndex ++){
      var currentDate = new Date();
      // Get the date of 7 days before the current date
      var sevenDaysAgo = new Date(currentDate.getTime() - (6 * 24 * 60 * 60 * 1000));
      var newNextDay = new Date(sevenDaysAgo.getTime() + (bIndex * 24 * 60 * 60 * 1000));
      var bDate = newNextDay.toISOString().slice(0, 10);
      areaChartData.labels.push(bDate);

      // filter data 
      for(let i = 0; i < paramRes.length; i ++){
        if(paramRes[i].paymentDate == bDate){

          areaChartData.datasets[0].data[bIndex] += paramRes[i].amount;

        }
      }
    }

    // Create bar chart
    var barChartCanvasDaily = $('#bar-chart-daily').get(0).getContext('2d');
    var barChartDataDaily = $.extend(true, {}, areaChartData);
    barChartDataDaily.datasets[0] = areaChartData.datasets[0];
    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }
    new Chart(barChartCanvasDaily, {
        type: 'bar',
        data: barChartDataDaily,
        options: barChartOptions
    })
  }
  // Hàm load dữ liệu tổng tiền đơn hàng theo tuần vào bar chart
  function loadResToBarChartMonth(paramRes){
    "use strict"
    var currentDate = new Date();

    for (var i = 2; i > -1; i--) {
      var month = currentDate.getMonth();
      currentDate.setMonth(month - 1);
      areaChartDataMonth.labels[i] = month + 1;
      // Lọc dữ liệu
      for(let bIndex = 0; bIndex < paramRes.length; bIndex ++){
        if(paramRes[bIndex][0] == month + 1){
          areaChartDataMonth.datasets[0].data[i] = paramRes[bIndex][1];
        }
      }
    }
    // Create bar chart
    var barChartCanvasMonthly = $('#bar-chart-monthly').get(0).getContext('2d');
    var barChartDataMonthly = $.extend(true, {}, areaChartDataMonth);
    barChartDataMonthly.datasets[0] = areaChartDataMonth.datasets[0];
    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }
    new Chart(barChartCanvasMonthly, {
        type: 'bar',
        data: barChartDataMonthly,
        options: barChartOptions
    })
  }

  // Hàm load dữ liệu vào biểu đồ tổng tiền đã mua
  function loadResToBarChartLevelCustomer(paramRes){
    "use strict"
    //-------------
    //- BAR CHART - User
    //-------------
    var bar_data_user = {
      data : [[1,0], [2,0], [3,0], [4,0]],
      bars: { show: true }
    }
    for(let bIndex = 0; bIndex < paramRes.length; bIndex ++){
      if(paramRes[bIndex][0] > 5000){
        bar_data_user.data[0][1] += 1;
      }
      if(paramRes[bIndex][0] > 2000 && paramRes[bIndex][0] <= 5000){
        bar_data_user.data[1][1] += 1;
      }
      if(paramRes[bIndex][0] > 1000 && paramRes[bIndex][0] <= 2000){
        bar_data_user.data[2][1] += 1;
      }
      if(paramRes[bIndex][0] > 500 && paramRes[bIndex][0] <= 1000){
        bar_data_user.data[3][1] += 1;
      }
    }
    // Load data to bar
    $.plot('#bar-chart-user', [bar_data_user], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
         bars: {
          show: true, barWidth: 0.5, align: 'center',
        },
      },
      colors: ['#ffc107'],
      xaxis : {
        ticks: [[1,'Platinum'], [2,'Gold'], [3,'Silver'], [4,'Vip']]
      }
    })
  }
  // Hàm load dữ liệu vào biểu đồ số lượng đơn hàng theo khách hàng
  function loadResToBarChartOrderQuantity(paramRes){
    "use strict"
    //-------------
    //- BAR CHART - Order
    //-------------
    let bar_data_order = {
      data : [[1,0], [2,0], [3,0], [4,0],[4,0]],
      bars: { show: true }
    }
    let vChartData = {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
         bars: {
          show: true, barWidth: 0.5, align: 'center',
        },
      },
      colors: ['#dc3545'],
      xaxis : {
        ticks: [[1,'user1'], [2,'user2'], [3,'user3'], [4,'user4']]
      }
    }

    for(let bIndex = 0; bIndex < 4; bIndex ++){
      bar_data_order.data[bIndex][1] = paramRes[bIndex][0];
      // lấy thông tin khách hàng
      $.ajax({
        url: gUrlCustomer + "/" + paramRes[bIndex][1],
        type: "GET",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + vtoken + "" 
          },
        success: function(res) {
          vChartData.xaxis.ticks[bIndex][1] = res.username;
          $.plot('#bar-chart-order', [bar_data_order], vChartData)
        },
        error: function(er) {
        }
      });
    }
  }

});