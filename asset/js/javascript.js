$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const vtoken = getCookie("token");

    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gUrlProduct = "http://localhost:8080/shop24h/api/noauth/products";

    var gUrlProductByProductLine = "http://localhost:8080/shop24h/api/noauth/products/product-line/";

    var gUrlProductByProductName = "http://localhost:8080/shop24h/api/noauth/products/product-name/";

    var gUrlWishlist = "http://localhost:8080/shop24h/wishlists";

    var gUrlOffice = "http://localhost:8080/shop24h/api/noauth/offices";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";
    
    var gNumberProduct = $("#number-product");
    
    var gUser = localStorage.getItem("user");

    var gProductListId = JSON.parse(localStorage.getItem("productList"));

    const gSrTop = ScrollReveal({
        origin: "top",
        distance : "80px",
        duration : 1000,
        reset: false
    })
    var gProductLineId = "all";

    var gSearch = "all"
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Tạo sự kiện nhấn nút tìm kiếm sản phẩm
    $(document).on("input change propertychange paste keyup", "#inp-search", function(){
        onInpSearchChange();
    })
    // Sự kiện nhấn nút đăng xuất tk
    $(document).on("click", "#btn-signout", function(){
        redirectToLogin();
        window.location.href = "./login.html"
    })
    // Tạo sự kiện nhấn mục user
    $(document).on("click", "#icon-user", function(){
        $("#user-info").removeClass("d-none");
        $("#icon-user").addClass("open");
    })
    // Tạo sự kiện nhấn mục user
    $(document).on("click", "#icon-user.open", function(){
        $("#user-info").addClass("d-none");
        $("#icon-user").removeClass("open");
    })
    // Thêm sản phẩm vào giỏ hàng
    $(document).on("click", "#btn-add", function(){
        onBtnAddToCartClick(this);
    })
    // Tạo sự kiện nhấn vào nút giỏ hàng
    $(document).on("click", "#icon-cart", function(){
        window.location.href = "./cart.html"
    })
    // Tạo sự kiện nhấn icon thêm sản phẩm vào danh sách yêu thích
    $(document).on("click", ".unfavourite-icon", function(){
        onBtnAddWishListClick(this);
    })
    // Tạo sự kiện nhấn icon bỏ sản phẩm yêu thích
    $(document).on("click", ".favourite-icon", function(){
        onBtnRemoveWishListClick(this);
    })
    // Tạo sự kiện nhấn vào các dòng sản phẩm
    $(document).on("click", "#headphone-filter", function(){
        $(".product-filter").removeClass("active");
        $("#headphone-filter").addClass("active");
        gProductLineId = 1;
        callApiSearchProductByNameAndCategory();
    })
    $(document).on("click", "#laptop-filter", function(){
        $(".product-filter").removeClass("active");
        $("#laptop-filter").addClass("active");
        gProductLineId = 2;
        callApiSearchProductByNameAndCategory();
    })
    $(document).on("click", "#tv-filter", function(){
        $(".product-filter").removeClass("active");
        $("#tv-filter").addClass("active");
        gProductLineId = 3;
        callApiSearchProductByNameAndCategory();
    })
    $(document).on("click", "#speaker-filter", function(){
        $(".product-filter").removeClass("active");
        $("#speaker-filter").addClass("active");
        gProductLineId = 4;
        callApiSearchProductByNameAndCategory();
    })
    $(document).on("click", "#camera-filter", function(){
        $(".product-filter").removeClass("active");
        $("#camera-filter").addClass("active");
        gProductLineId = 5;
        callApiSearchProductByNameAndCategory();
    })
    $(document).on("click", "#game-filter", function(){
        $(".product-filter").removeClass("active");
        $("#game-filter").addClass("active");
        gProductLineId = 6;
        callApiSearchProductByNameAndCategory();
    })
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();

        callApiGetAllproducts();

        callApiGetAllOffice();

        gSrTop.reveal(".hero");
        
    }

    // Hàm xử lí tìm kiếm
    function onInpSearchChange(){
        "use strict"
        
        gSearch = $("#inp-search").val().trim();

        if(gSearch == ""){
            gSearch = "all";
        }
        callApiSearchProductByNameAndCategory();
    }
    // Hàm xử lí thêm sản phẩm yêu thích
    function onBtnAddWishListClick(paramIcon){
        "use strict"
        var vProductId = $(paramIcon).data("id");
        if(gUser != -1){
            callApiCreateWishItem(vProductId);
        }
    }
    // Hàm xử lí bỏ sản phẩm yêu thích
    function onBtnRemoveWishListClick(paramIcon){
        "use strict"
        var vProductId = $(paramIcon).data("id");
        if(gUser != -1){
            callApiDeleteWishItem(vProductId);
        }
    }
    // Hàm thêm sản phẩm vào giỏ hàng
    function onBtnAddToCartClick(paramE){
        "use strict"
        
        var vProductObj = {
            id: "",
            quantity_order: 1,
            priceEach: 0
        }
        var vQuantityStock = $(paramE).attr("data-stock");

        readProductData(vProductObj,paramE,vQuantityStock);

        var bIndex = 0;
        var vFound = false;

        if (gProductListId == null){
            gProductListId = [];
        }

        while(vFound == false &&  bIndex < gProductListId.length){
            if(gProductListId[bIndex].id == vProductObj.id){
                if (gProductListId[bIndex].quantity_order < vQuantityStock){
                    gProductListId[bIndex].quantity_order = gProductListId[bIndex].quantity_order + 1;
                }
                vFound = true;
            }
            else {
                bIndex ++;
            }
        }
         if (vFound == false){
            gProductListId.push(vProductObj);
         }

        gNumberProduct.html(gProductListId.length);

        localStorage.setItem("productList",JSON.stringify(gProductListId));
    }
    // Hàm kiểm tra người dùng đăng nhập
    function callApiCheckUser(){
        "use strict"
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function(responseObject) {
                if(gUser != responseObject.id){
                    localStorage.clear();
                    localStorage.setItem("user", responseObject.id);
                    gProductListId = [];
                }
                responseHandler(responseObject);
            },
            error: function(xhr) {
                redirectToLogin();
                if(gUser != "-1"){
                    localStorage.clear();
                    localStorage.setItem("user", "-1");
                    gProductListId = [];
                }
                if(gProductListId != null){
                    gNumberProduct.html(gProductListId.length);
                }
            }
        });
    }
    // Hàm thêm sản phẩm yêu thích cho người dùng
    function callApiCreateWishItem(paramProductId){
        "use strict"
        $.ajax({
            url: gUrlWishlist + "?productId=" + paramProductId,
            type: "POST",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function() {
                $("#product-wish-"+ paramProductId +"").html(`
                <i data-id = "`+ paramProductId +`" class="fa-solid fa-heart favourite-icon"></i>
                `)
            },
            error: function() {
            }
        });
    }
    // Hàm xoá sản phẩm yêu thích cho người dùng
    function callApiDeleteWishItem(paramProductId){
        "use strict"
        $.ajax({
            url: gUrlWishlist + "?productId=" + paramProductId,
            type: "DELETE",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function() {
                $("#product-wish-"+ paramProductId +"").html(`
                <i data-id = "`+ paramProductId +`" class="fa-regular fa-heart unfavourite-icon"></i>
                `)
            },
            error: function() {

            }
        });
    }
    // Hàm gọi api lấy toàn bộ danh sách yêu thích
    function callApiGetWishList(){
        "use strict"
        $.ajax({
            url: gUrlWishlist,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
                },
            success: function(responseObject) {
                wishListHandler(responseObject);
            },
            error: function() {
            }
        });
    }
    // Hàm gọi api lấy toàn bộ sản phẩm
    function callApiGetAllproducts(){
        "use strict"
        $.ajax({
            url: gUrlProduct,
            type: "GET",
            dataType: "json",
            success: function(responseObject) {
                loadProductToPage(responseObject);
                loadProductToCarosel(responseObject);
            },
            error: function() {
            }
        });
    }
    // Hàm gọi api lấy toàn bộ office
    function callApiGetAllOffice(){
        "use strict"
        $.ajax({
            url: gUrlOffice,
            type: "GET",
            dataType: "json",
            success: function(responseObject){
                loadOfficeToPage(responseObject);
            },
            error: function() {
            }
        });
    }
    // Hàm gọi api lấy sản phẩm qua tên sản phẩm
    function callApiSearchProductByNameAndCategory(){
        "use strict"
        $.ajax({
            url: gUrlProductByProductName + gSearch + "/product-line-id/" + gProductLineId,
            type: "GET",
            dataType: "json",
            success: function(responseObject) {
                loadProductToPage(responseObject);
            },
            error: function() {
            }
        });
    }
    // Hàm gọi api lấy sản phẩm qua dòng sản phẩm
    function callApiGetProductByProductLine(paramProductLine){
        "use strict"
        var vProductLineId = "";

        if (paramProductLine == "headphone"){
            vProductLineId = 1;
        }
        if (paramProductLine == "laptop"){
            vProductLineId = 2;
        }
        if (paramProductLine == "television"){
            vProductLineId = 3;
        }
        if (paramProductLine == "speaker"){
            vProductLineId = 4;
        }
        if (paramProductLine == "camera"){
            vProductLineId = 5;
        }
        if (paramProductLine == "game"){
            vProductLineId = 6;
        }
        $.ajax({
            url: gUrlProductByProductLine + vProductLineId,
            type: "GET",
            dataType: "json",
            success: function(responseObject) {
                loadProductToPage(responseObject);
            },
            error: function(er) {
            }
        });
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    // Hàm hiển thị dữ liệu sản phẩm lên trang
    function loadProductToPage(paramRes){
        "use strict"
        var vProductContain = $("#list-product");
        vProductContain.empty();

        

        var vLength = paramRes.length;
        if(vLength > 12){
            vLength = 12
        }

        for(var bIndex = 0; bIndex < vLength; bIndex ++){
            var bDisable = "";
            if (paramRes[bIndex].quantityInStock < 1){
                bDisable = "disabled";
            }
            var vPrice = `<p class="price"><span class="discount">$` + paramRes[bIndex].buyPrice + `</span> $` + paramRes[bIndex].discountPrice + `</p>`

            if(paramRes[bIndex].discountPrice >= paramRes[bIndex].buyPrice ){
                vPrice = `<p class="price text-dark">$` + paramRes[bIndex].discountPrice + `</p>`
            }
            var bProduct = `<div class="col-6 col-lg-3 col-md-4 col-sm-4">
            <div class="item">
                <a href="./product-details.html?id=` + paramRes[bIndex].id + `">
                    <img src="` + gUrlDownloadPhoto +"/"+ paramRes[bIndex].photo1 + `" alt="` + paramRes[bIndex].productName + `"/>
                </a>
                <p class="product-name line-clamp-1">` + paramRes[bIndex].productName + `</p>`
                + vPrice + `
                <p id = "stock-id-` + paramRes[bIndex].id + `" class="stock">Quantity in stock: ` + paramRes[bIndex].quantityInStock + ` </p>
                <div class="cart-block">
                    <button ` + bDisable + ` id="btn-add" data-stock = "` + paramRes[bIndex].quantityInStock 
                    + `" data-price = "` + paramRes[bIndex].discountPrice + `" data-id = "` 
                    + paramRes[bIndex].id + `" class="btn-add-cart"><i class="fa-solid fa-cart-plus"></i></button>
                    <div class="wish-list-block" id = "product-wish-`+ paramRes[bIndex].id +`">
                        <i data-id = "`+ paramRes[bIndex].id +`" class="fa-regular fa-heart unfavourite-icon"></i>
                    </div>
                </div>
            </div>
            </div>`
            vProductContain.append(bProduct);
        }
        callApiGetWishList();
        gSrTop.reveal(".item",{interval: 200});
    }
    // Hàm hiển thị dữ liệu sản phẩm lên carousel
    function loadProductToCarosel(paramRes){
        "use strict"
        var vCarouselContain = $("#carousel-container");
        vCarouselContain.empty();

        var vLength = paramRes.length;
        if(vLength > 3){
            vLength = 4
        }

        for(var bIndex = 0; bIndex < vLength; bIndex ++){
            let bFirstActive = "";
            if(bIndex == 0){
                bFirstActive = "active"
            }
            else {
                bFirstActive = "";
            }

            var bCarouselItem = `
                <div class="carousel-item ` + bFirstActive + `">
                    <div class="info">
                        <div class="caption">
                            <p class="sub-title">`+ paramRes[bIndex].productVendor +`</p>
                            <h2 class="title line-clamp-1">`+ paramRes[bIndex].productName +`</h2>
                            <p class="desc line-clamp">` + paramRes[bIndex].productDescription + `</p>
                            <a href="./list-product.html" class="btn-shop">SHOP NOW</a>
                        </div>
                        <img src="`+gUrlDownloadPhoto + "/" + paramRes[bIndex].photo1 +`" alt="Product">
                    </div>
                </div>`
            vCarouselContain.append(bCarouselItem);
        }
    }

    // Hàm hiển thị thông tin văn phòng
    function loadOfficeToPage(paramRes){
        "use strict"
        var vOfficeContain = $("#store-list");
        vOfficeContain.empty();

        for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
            let bOfficeItem = `
            <div class="col-12 col-sm-6 col-md-3 mt-5">
                <div class="store-item">
                    <p class="store-title"><i class="fa-solid fa-store"></i> Stores - `+ (bIndex + 1) +`</p>
                    <p class="address"><i class="fa-solid fa-location-dot"></i> ` + paramRes[bIndex].address_line + ", " + paramRes[bIndex].city + `</p>
                    <p class="phone"><i class="fa-solid fa-phone-volume"></i> `+ paramRes[bIndex].phone +`</p>
                </div>
            </div>`

            vOfficeContain.append(bOfficeItem);
        }
        gSrTop.reveal(".store-item", {interval: 200});
    }
    // Hàm xử lí danh sách wishList;
    function wishListHandler(paramRes){
        "use strict"
        paramRes.forEach(wishItem => {
            $("#product-wish-"+ wishItem.product.id +"").html(`
            <i data-id = "`+ wishItem.product.id +`" class="fa-solid fa-heart favourite-icon"></i>
            `)
        });
    }
    //Hàm thu thập dữ liệu khi nhấn thêm sản phẩm
    function readProductData(paramObj, paramE){
        "use strict"
        paramObj.id = $(paramE).attr("data-id");
        paramObj.priceEach = $(paramE).attr("data-price");
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau khi đăng nhập
    function responseHandler(paramRes){
        "use strict"
        $("#icon-login").addClass("d-none");
        $("#icon-user").removeClass("d-none");
        if(gProductListId != null){

            gNumberProduct.html(gProductListId.length);
        }
    }

    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }

});