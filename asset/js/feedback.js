$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo url
    var gUrlFeedback = "http://localhost:8080/shop24h/feedbacks";

    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";

    // Lấy token
    const vtoken = getCookie("token");

    // Cấu hình data table
    var vDataTable = $('#feedback-table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            columns: [
                {data: "id"},
                {data: "action"},
                {data: "rating"},
                {data: "comments"},
                {data: "feedbackDate"},
                {data: "customer.username"},
                {data: "product.productName"},
            ],
            columnDefs: [
                {
                    targets: 1,
                    defaultContent: `<i id = "delete-feedback" class="fas fa-trash-alt delete-icon" style ="color:red;"></i>`
                },
                {
                    targets: 2,
                   render: function(data){
                    if(data == 1){
                        return `<i class="fas fa-star text-warning"></i>`
                    }
                    if(data == 2){
                        return `<i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>`
                    }
                    if(data == 3){
                        return `<i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>`
                    }
                    if(data == 4){
                        return `<i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>`
                    }
                    if (data == 5){
                        return `<i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>
                                <i class="fas fa-star text-warning"></i>`
                    }
                   }
                }
            ]
    });
    // Lưu id feedback dc chọn
    var gIdFeedback = ""; 
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Tạo sự kiện logout
    $(document).on("click", "#btn-logout", function(){
        redirectToLogin();
    })
    // Tạo sự kiến nhấn nút xoá feedback
    $(document).on("click", "#feedback-table tr .delete-icon", function(){
        onIconDeleteRowClick(this);
    })
    // Tạo sự kiến nhấn xác nhận xoá feedback
    $(document).on("click", "#btn-delete", function(){
        onBtnDeleteProductClick();
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();
    }
    // Hàm gọi api lấy toàn bộ feedback
    function callApiGetAllFeedback(){
        $.ajax({
            url: gUrlFeedback+ "/all",
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                toastr.success("Get feedbacks successfully");
                loadFeedbackToTable(responseObject);
            },
            error: function(er) {
                toastr.error("Error! fail to get data");
            }
        });
    }
    // Hàm xác nhận xoá feedback
    function onBtnDeleteProductClick(){
        "use strict"
        $("#modal-delete").modal("hide");
        callApiDeleteProduct();
    }
    // Hàm xử lí nhấn incon xoá feedback
    function onIconDeleteRowClick(paramIcon){
        gIdFeedback = "";
        var vDataRow = onRowClick(paramIcon);
        gIdFeedback = vDataRow[0].id;
        $("#modal-delete").modal("show");
    }
    // Hàm xoá feedback
    function callApiDeleteProduct(){
        "use strict"
        $.ajax({
            url: gUrlFeedback + "/" + gIdFeedback,
            type: "DELETE",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Delete the feedback successfully');
                callApiGetAllFeedback();
            },
            error: function() {
                toastr.error('Error! Please try again.');
            }
        });
    }
    // Hàm kiểm tra đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                if(responseObject.roles[0].name == "ROLE_MANAGER"){
                    callApiGetAllFeedback();
                    responseHandler(responseObject);
                }
                else{
                    redirectToLogin();
                }
            },
            error: function(xhr) {
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau kiểm tra người dùng thành công
    function responseHandler(paramRes){
        "use strict"
        $("#username").html(paramRes.username);
        $("#user-img").attr("src", gUrlDownloadPhoto +"/"+ paramRes.photo);
    }
    // Hàm ghi dữ liệu vào bảng
    function loadFeedbackToTable(paramRes){
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(paramRes);
        vDataTable.order( [ 0, 'desc' ] ).draw();
    }
    // Hàm lấy dữ liệu khi chọn row
    function onRowClick(paramRow){
        "use strict"
        var vRow = $(paramRow).closest("tr");
        $("#feedback-table tr").removeClass("active");
        vRow.addClass("active");
        var vDataOnRow = vDataTable.rows(vRow).data();
        return vDataOnRow;
    }
})