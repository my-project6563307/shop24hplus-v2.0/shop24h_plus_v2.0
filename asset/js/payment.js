$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo url
    var gUrlPayment = "http://localhost:8080/shop24h/payments";

    var gUrlCustomer = "http://localhost:8080/shop24h/customers";

    var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

    var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";

    // Lấy token
    const vtoken = getCookie("token");

    // Cấu hình data table
    var vDataTable = $('#payment-table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            columns: [
                {data: "id"},
                {data: "amount"},
                {data: "paymentDate"},
                {data: "customer.username"},
            ]
    });
    // Lưu id sản phẩm dc chọn
    var gIdPayment = ""; 
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();

    // Tạo sự kiện logout
    $(document).on("click", "#btn-logout", function(){
        redirectToLogin();
    })
    
    // Tạo sự kiện nhấn dòng trên bảng
    $(document).on("click", "#payment-table td", function(){
        onBtnRowClick(this);
    })
    // Tạo sự kiến nhấn nút thêm payment
    $(document).on("click", "#btn-add", function(){
        onBtnAddClick();
    })
    // Tạo sự kiến nhấn nút cập nhật payment
    $(document).on("click", "#btn-add-payment", function(){
        onBtnAddPaymentClick();
    })
    // Tạo sự kiến nhấn nút cập nhật payment
    $(document).on("click", "#btn-update", function(){
        onBtnUpdatePaymentClick();
    })

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading(){
        "use strict"
        callApiCheckUser();
    }
    // Hàm gọi api lấy toàn bộ hoá đơn
    function callApiGetAllPayment(){
        $.ajax({
            url: gUrlPayment,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                toastr.success("Get payments successfully");
                loadPaymentToTable(responseObject);
            },
            error: function(er) {
                toastr.error("Error! fail to get data");
            }
        });
    }
    // Hàm gọi api lấy toàn bộ customer
    function callApiGetAllCustomer(){
        $.ajax({
            url: gUrlCustomer,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                loadCustomerToSelect(responseObject);
            },
            error: function(er) {
            }
        });
    }
    // Hàm tạo mới payment
    function callApiCreatePayment(paramObj){
        "use strict"
        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: gUrlPayment + "?customerId=" + paramObj.customerId,
            type: "POST",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Create a new payment successully');
                callApiGetAllPayment();
            },
            error: function() {
                toastr.error('Error during create! Please try again.');
            }
        });
    }
    // Hàm cập nhật payment
    function callApiUpdatePayment(paramObj){
        "use strict"
        var vUrl = "http://localhost:8080/shop24h/payments/" + gIdPayment;

        var vJsonObj = JSON.stringify(paramObj);
        $.ajax({
            url: vUrl + "?customerId=" + paramObj.customerId,
            type: "PUT",
            contentType: "application/json",
            data: vJsonObj,
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function() {
                toastr.success('Update the payment successully');
                callApiGetAllPayment();
            },
            error: function() {
                toastr.error('Error during update! Please try again.');
            }
        });
    }
    // Hàm cập nhật payment
    function onBtnUpdatePaymentClick(){
        "use strict"
        var vAddObj = {
            amount: "",
            paymentDate: "",
            customerId: "",
        }
        // Thu thập dữ liệu
        readFormData(vAddObj);
        // Xử lí dữ liệu
        var vValid = validateData(vAddObj);
        if(vValid){
            callApiUpdatePayment(vAddObj);
        }
    }
    // Hàm kiểm tra đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                if(responseObject.roles[0].name == "ROLE_MANAGER"){
                    callApiGetAllCustomer();
                    callApiGetAllPayment();
                    responseHandler(responseObject);
                }
                else{
                    redirectToLogin();
                }
            },
            error: function(xhr) {
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
    }
    // Hàm xử lí khi nhấn row
    function onBtnRowClick(paramRow){
        "use strict"
        gIdPayment = "";
        var vDataRow = onRowClick(paramRow);
        if(vDataRow.length > 0){
            loadDataToForm(vDataRow);
        }
    }

    // Hàm xử lí khi nhấn thêm payment
    function onBtnAddClick(){
        "use strict"
        emptyForm();
    }
    // Hàm xử lí khi nhấn thêm payment
    function onBtnAddPaymentClick(){
        "use strict"
        var vAddObj = {
            amount: "",
            paymentDate: "",
            customerId: "",
        }
        // Thu thập dữ liệu
        readFormData(vAddObj);
        // Xử lí dữ liệu
        var vValid = validateData(vAddObj);
        if(vValid){
            callApiCreatePayment(vAddObj);
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu customer vào ô select
    function loadCustomerToSelect(paramRes){
        "use strict"
        var vSelectBoxCustomer = $("#select-customer");
        for(let bIndex = 0; bIndex < paramRes.length; bIndex ++){
            let bOption = $("<option>");
            bOption.prop("value", paramRes[bIndex].id);
            bOption.prop("text", paramRes[bIndex].username + "-" + paramRes[bIndex].phoneNumber );

            vSelectBoxCustomer.append(bOption);
        }
    }
    // Hàm thu thập dữ liệu tạo mới 
    function readFormData(paramObj){
        "use strict"
        paramObj.amount = $("#inp-amount").val();
        paramObj.paymentDate = $("#inp-payment-date").val();
        paramObj.customerId = $("#select-customer").val();
    }
    // Hàm xử lí dữ liệu
    function validateData(paramObj){
        "use strict"
        if(paramObj.amount == ""){
            toastr.error('Please fill out amount');
            $("#inp-amount").focus();
            return false;
        }
        if(paramObj.paymentDate == ""){
            toastr.error('Please fill out payment date');
            $("#inp-payment-date").focus();
            return false;
        }
        if(paramObj.customerId == ""){
            toastr.error('Please select a customer');
            $("#select-customer").focus();
            return false;
        }

    return true;
    }
    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí sau kiểm tra người dùng thành công
    function responseHandler(paramRes){
        "use strict"
        $("#username").html(paramRes.username);
        $("#user-img").attr("src", gUrlDownloadPhoto +"/"+ paramRes.photo);
    }
    // Hàm ghi dữ liệu vào bảng
    function loadPaymentToTable(paramRes){
        "use strict"
        vDataTable.clear();
        vDataTable.rows.add(paramRes);
        vDataTable.order( [ 0, 'desc' ] ).draw();
    }

    // Hàm lấy dữ liệu khi chọn row
    function onRowClick(paramRow){
        "use strict"
        var vRow = $(paramRow).closest("tr");
        $("#payment-table tr").removeClass("active");
        vRow.addClass("active");
        var vDataOnRow = vDataTable.rows(vRow).data();
        return vDataOnRow;
    }
    // Hàm tải dữ liệu lên form
    function loadDataToForm(paramRowData){
        "use strict"
        $("#btn-action").html(`<button id="btn-update" class="btn btn-primary">Update</button>`);
        gIdPayment = paramRowData[0].id;
        $("#inp-id").removeClass("d-none");
        $("#label-id").removeClass("d-none");
        $("#inp-id").val(paramRowData[0].id);

        $("#inp-check-number").val(paramRowData[0].checkNumber);
        $("#inp-amount").val(paramRowData[0].amount);
        $("#inp-payment-date").val(paramRowData[0].paymentDate);
        $("#select-customer").val(paramRowData[0].customer.id);
    }
    // Hàm xoá dữ liệu trên form
    function emptyForm(){
        "use strict"
        $("#btn-action").html(`<button id="btn-add-payment" class="btn btn-info">Add</button>`);
        gIdPayment = "";
        $("#inp-id").addClass("d-none");
        $("#label-id").addClass("d-none");
        $("#inp-id").val("");

        $("#inp-check-number").val("");
        $("#inp-amount").val("");
        $("#inp-payment-date").val("");
        $("#select-customer").val("");
    }
})