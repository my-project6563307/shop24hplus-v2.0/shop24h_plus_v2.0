$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");
        var urlInfo = "http://localhost:8080/shop24h/api/noauth/customers";

        var gOrderByCustomerUrl = "http://localhost:8080/shop24h/orders/customer";

        var gOrderByStatusUrl = "http://localhost:8080/shop24h/orders/status";

        var gOrderDetailsUrl = "http://localhost:8080/shop24h/order-details/orders";

        var gOrderCancelUrl = "http://localhost:8080/shop24h/orders/cancel";

        var gUrlOffice = "http://localhost:8080/shop24h/api/noauth/offices";

        var gUrlDownloadPhoto = "http://localhost:8080/shop24h/api/noauth/downloadFile";
    
        var gNumberProduct = $("#number-product");
    
        var gUser = localStorage.getItem("user");
    
        var gProductListId = JSON.parse(localStorage.getItem("productList"));
    
        var gOrderIdSelected = "";

        const gSrTop = ScrollReveal({
            origin: "left",
            distance : "60px",
            duration : 1000,
            reset: false
        })
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        // Sự kiện load trang
        onPageLoading();
    
        $(document).on("click", "#btn-signout", function(){
            redirectToLogin();
            window.location.href = "./login.html"
        })
        $(document).on("click", "#icon-user", function(){
            $("#user-info").toggleClass("d-none");
        })

        // Tạo sự kiện nhấn vào các mục phân loại order
        $(document).on("click", "#all-order", function(){
            $(".order-header h4").removeClass("active");
            $("#all-order").toggleClass("active");
            callApiGetOrdersByCustomer();
        })
        $(document).on("click", "#onhold", function(){
            $(".order-header h4").removeClass("active");
            $("#onhold").toggleClass("active");
            callApiGetOrderByStatus("On hold");
        })
        $(document).on("click", "#delivering", function(){
            $(".order-header h4").removeClass("active");
            $("#delivering").toggleClass("active");
            callApiGetOrderByStatus("Delivering");
        })
        $(document).on("click", "#shipped", function(){
            $(".order-header h4").removeClass("active");
            $("#shipped").toggleClass("active");
            callApiGetOrderByStatus("Shipped");
        })
        $(document).on("click", "#canceled", function(){
            $(".order-header h4").removeClass("active");
            $("#canceled").toggleClass("active");
            callApiGetOrderByStatus("Canceled");
        })

        // Tạo sự kiện nhấn nút huỷ đơn hàng
        $(document).on("click", ".btn-cancel", function(){
            onBtnCancelClick(this);
        })
        // Tạo sự kiện nhấn nút xác nhận huỷ đơn hàng
        $(document).on("click", "#btn-confirm-cancel", function(){
            $("#modal-cancel").toggleClass("d-none");
            callApiCancelOrder(gOrderIdSelected);
        })
        // Tạo sự kiện nhấn nút chi tiết đơn hàng
        $(document).on("click", ".icon-details", function(){
            onIconDetailsClick(this);
        })
        // Tạo sự kiện đóng modal
        $(document).on("click", ".close", function(){
            $("#details-modal").toggleClass("d-none");
        })
        // Tạo sự kiện đóng modal
        $(document).on("click", "#icon-close-modal-cancel", function(){
            $("#modal-cancel").toggleClass("d-none");
        })

        // Tạo sự kiện nhấn nút huỷ đơn 
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiCheckUser();

            callApiGetAllOffice();

            gSrTop.reveal(".path");
            gSrTop.reveal(".user-header", {delay: 200});
            gSrTop.reveal(".user-body", {delay: 300});
        }

        //  Hàm xử lí sự kiện huỷ đơn hàng
        function onBtnCancelClick(paramE){
            "use strict"
            gOrderIdSelected = $(paramE).data("id");
            $("#modal-cancel").removeClass("d-none");
        }

        //  Hàm xử lí sự kiện chi tiết đơn hàng
        function onIconDetailsClick(paramE){
            "use strict"
            let vOrderId = $(paramE).data("id");
            callApiGetOrderDetailsByOrderId(vOrderId);
        }
        // Hàm kiểm tra người dùng đăng nhập
        function callApiCheckUser(){
            $.ajax({
                url: urlInfo,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    if(gUser != responseObject.id){
                        localStorage.clear();
                        localStorage.setItem("user", responseObject.id);
                        gProductListId = [];
                    }
                    responseHandler(responseObject);
                    callApiGetOrdersByCustomer();
                },
                error: function() {
                    redirectToLogin();
                    if(gUser != "-1"){
                        localStorage.clear();
                        localStorage.setItem("user", "-1");
                        gProductListId = [];
                    }
                    if (gProductListId != null){
                        gNumberProduct.html(gProductListId.length);
                    }
                }
            });
        }
        // Hàm gọi api lấy dữ liệu order
        function callApiGetOrdersByCustomer(){
            $.ajax({
                url: gOrderByCustomerUrl,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    loadOrderToOrderList(responseObject);
                },
                error: function() {
                }
            });
        }
        // Hàm gọi api huỷ order
        function callApiCancelOrder(paramId){
            $.ajax({
                url: gOrderCancelUrl + "/" + paramId,
                type: "PUT",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    $('.toast').toast('show');
                    callApiGetOrdersByCustomer();
                },
                error: function() {
                }
            });
        }

        // Hàm gọi api lấy chi tiết đơn hàng
        function callApiGetOrderDetailsByOrderId(paramId){
            $.ajax({
                url: gOrderDetailsUrl + "/" + paramId,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    loadOrderDetailsToModal(responseObject);
                },
                error: function() {
                }
            });
        }
        // Hàm gọi api lấy dữ liệu order bằng status
        function callApiGetOrderByStatus(paramStatus){
            $.ajax({
                url: gOrderByStatusUrl + "?status=" + paramStatus,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                    },
                success: function(responseObject) {
                    loadOrderToOrderList(responseObject);
                },
                error: function() {
                }
            });
        }
        // Hàm gọi api lấy toàn bộ office
        function callApiGetAllOffice(){
            "use strict"
            $.ajax({
                url: gUrlOffice,
                type: "GET",
                dataType: "json",
                success: function(responseObject){
                    loadOfficeToPage(responseObject);
                },
                error: function() {
                }
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm tải dữ liệu lên mục đơn hàng
        function loadOrderToOrderList(paramRes){
            "use strict"
            let vOrderContainer = $("#order-container");
            vOrderContainer.empty();

            for(let bIndex = 0; bIndex < paramRes.length; bIndex ++){

                let vCancel = "";
                if (paramRes[bIndex].status == "On hold"){
                    vCancel = `<button data-id = "` + paramRes[bIndex].id + `" class="btn btn-danger btn-cancel">Cancel</button>`
                }

                let bRequiredDate = "";
                if (paramRes[bIndex].requiredDate != null){
                    bRequiredDate = paramRes[bIndex].requiredDate;
                }

                let shippedDate = "";
                if (paramRes[bIndex].shippedDate != null){
                    shippedDate = paramRes[bIndex].shippedDate;
                }

                let vOrderItem = `
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="item-order">
                        <div class="order-details block-id text-info">
                            <p><i class="fa-solid fa-play"></i> ID:</p>
                            <p class="id-order">` + paramRes[bIndex].id + `</p>
                        </div>
                        <div class="order-details block-status">
                            <p><i class="fa-solid fa-truck-arrow-right"></i> Status:</p>
                            <p class="text-info">` + paramRes[bIndex].status + `</p>
                        </div>
                        <div class="order-details block-order-date">
                            <p><i class="fa-solid fa-calendar-days"></i> Order date:</p>
                            <p class=" text-success">` + paramRes[bIndex].orderDate + `</p>
                        </div>
                        <div class="order-details block-required-date">
                            <p><i class="fa-solid fa-calendar-days"></i> Delivery date:</p>
                            <p class="  text-warning">` + bRequiredDate + `</p>
                        </div>
                        <div class="order-details block-shipped-date">
                            <p><i class="fa-solid fa-calendar-days"></i> Shipped date:</p>
                            <p class="  text-danger">` + shippedDate + `</p>
                        </div>
                        <div class="order-details block-btn">
                        <i data-id = "` + paramRes[bIndex].id + `" class="fa-solid fa-circle-info icon-details"></i>
                            ` + vCancel + `
                        </div>
                    </div>
                </div>`

                vOrderContainer.append(vOrderItem);
            }
            gSrTop.reveal(".item-order", {interval: 200});
        }

        // Hàm tải dữ liệu chi tiết đơn hàng lên modal 
        function loadOrderDetailsToModal(paramRes){
            "use strict"
            $("#details-modal").toggleClass("d-none");

            let vProductContainer = $("#product-container");
            vProductContainer.empty();

            for(let bIndex = 0; bIndex < paramRes.length; bIndex ++){
                let bProductItem = `
                    <div class="row product-item">
                        <div class="product-details col-12 col-sm-6 col-md-3">
                            <img src="` + gUrlDownloadPhoto + "/" + paramRes[bIndex].product.photo1 + `" alt="" class="product-img">
                        </div>
                        <div class="product-details col-12 col-sm-6 col-md-3">
                            <a href = "./product-details.html?id=`+ paramRes[bIndex].product.id +`" class="product-name text-info">` + paramRes[bIndex].product.productName + `</a>
                        </div>
                        <div class="product-details col-12 col-sm-6 col-md-3">
                            <p class="product-price">Price: $ <span class="text-danger">` + paramRes[bIndex].priceEach + `</span></p>
                        </div>
                        <div class="product-details col-12 col-sm-6 col-md-3">
                            <p class="product-quantity">Quantity: ` + paramRes[bIndex].quantity_order + `</p>
                        </div>
                    </div>`
                vProductContainer.append(bProductItem);
            }
        }
        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        // Hàm hiển thị thông tin văn phòng
        function loadOfficeToPage(paramRes){
            "use strict"
            var vOfficeContain = $("#store-list");
            vOfficeContain.empty();

            for(let bIndex = 0; bIndex < paramRes.length; bIndex++){
                let bOfficeItem = `
                <div class="col-12 col-sm-6 col-md-3 mt-5">
                    <div class="store-item">
                        <p class="store-title"><i class="fa-solid fa-store"></i> Stores - `+ (bIndex + 1) +`</p>
                        <p class="address"><i class="fa-solid fa-location-dot"></i> ` + paramRes[bIndex].address_line + ", " + paramRes[bIndex].city + `</p>
                        <p class="phone"><i class="fa-solid fa-phone-volume"></i> `+ paramRes[bIndex].phone +`</p>
                    </div>
                </div>`
                vOfficeContain.append(bOfficeItem);
            }
        }
        // Hàm xử lí sau khi đăng nhập
        function responseHandler(paramRes){
            "use strict"
            $("#icon-login").addClass("d-none");
            $("#icon-user").removeClass("d-none");
            if(gProductListId != null){
                gNumberProduct.html(gProductListId.length);
            }
            if(paramRes.photo != null){
                $("#user-img").attr("src", gUrlDownloadPhoto +"/"+ paramRes.photo);
            }
            $("#email-header").html(paramRes.email);
            $("#phone-header").html(paramRes.phoneNumber);
        }
    
        //Hàm logout
        function redirectToLogin() {
            // Trước khi logout cần xóa token đã lưu trong cookie
            setCookie("token", "", 1);
        }
        //Hàm setCookie
        function setCookie(cname, cvalue) {
            document.cookie = cname + "=" + cvalue + ";" + ";path=/";
        }

    });